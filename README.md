## Requirements
- Docker
- Composer

## Installation
- composer install
- ./vendor/bin/sail up
- php artisan migrate --seed

## Usage
- All the protected apis works based on key based authentication. So add your token as Bearer token.
- Use Loans end point to create, approve, view loan
- Use repayment endpoint to make a payment

```
  POST       api/auth/login .................................................... login › AuthController@login
  POST       api/auth/register ................................................. register › AuthController@register
  POST       api/loans ......................................................... loans.store › LoanController@store
  GET|HEAD   api/loans/{loan} .................................................. loans.show › LoanController@show
  POST       api/loans/{loan}/repayments ....................................... repayments.store › RepaymentController@store
  POST       api/loans/{loan}/status ........................................... loans.status.post › LoanController@changeStatus
```
