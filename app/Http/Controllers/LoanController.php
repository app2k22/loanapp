<?php

namespace App\Http\Controllers;

use App\Events\LoanCreatedEvent;
use App\Http\Requests\StoreLoanRequest;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Enums\LoanStatusEnum;

class LoanController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLoanRequest $request
     * @return Response
     */
    public function store(StoreLoanRequest $request): Response
    {
        $loanData = $request->validated();

        $loanData['user_id'] = auth()->user()->id;
        $loanData['status'] = LoanStatusEnum::PENDING->value;

        $loan = Loan::create($loanData);
        LoanCreatedEvent::dispatch($loan);

        return response([
            'data' => (new LoanResource($loan))
        ]);
    }

    /**
     * Change loan status to approved
     *
     * @param Request $request
     * @param Loan $loan
     * @return Response
     */
    public function changeStatus(Request $request, Loan $loan): Response
    {
        if($loan->status === LoanStatusEnum::APPROVED->value){
            return response([ 'message' => 'Loan already approved'], 422);
        }

        $loan->update([
            'status' => LoanStatusEnum::APPROVED->value,
            'approved_by' => auth()->user()->id,
            'approved_at' => \Carbon\Carbon::now()
        ]);

        return response([ 'data' => new LoanResource($loan)], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param \App\Models\Loan $loan
     * @return Response
     */
    public function show(Request $request, Loan $loan): Response
    {
        if ($request->user()->cannot('view', $loan)) {
            abort(403, 'You don\'t have access to this loan.');
        }

        return response([ 'data' => new LoanResource($loan)], 200);
    }
}
