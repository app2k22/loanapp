<?php

namespace App\Http\Controllers;

use App\Enums\LoanStatusEnum;
use App\Enums\RepaymentStatusEnum;
use App\Http\Requests\StoreRepaymentRequest;
use App\Http\Resources\LoanResource;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RepaymentController extends Controller
{
    /**
     * @param StoreRepaymentRequest $request
     * @param Loan $loan
     * @return void
     */
    public function store(StoreRepaymentRequest $request, Loan $loan): Response
    {
        if ($request->user()->cannot('view', $loan)) {
            abort(403, 'You don\'t have access to this loan.');
        }

        if($loan->status == LoanStatusEnum::PENDING->value){
            return response(['message' => 'Loan is not yet approved'], 403);
        }

        if($loan->status == LoanStatusEnum::PAID->value){
            return response(['message' => 'Loan already paid'], 403);
        }

        $amount = $request->validated()['amount'];

        $remainingAmount = round(($loan->amount - $loan->paid_amount), 2);

        if($amount >  $remainingAmount){
            return response(['message' => 'Amount is greater than remaining amount of '. $remainingAmount], 422);
        }

        $repayment = $loan->repayments()->where('status', RepaymentStatusEnum::PENDING->value)->orderBy('id', 'asc')->first();

        if($repayment){
            if( $amount >= $repayment->due_amount || $amount == $remainingAmount ){
                $repayment->pay($amount, $loan);
                return response((new LoanResource($loan)));
            } else {
                return response([
                    'message' => 'The amount should be greater than or equals to '. $repayment->due_amount
                ], 422);
            }
        }
    }
}
