<?php

namespace App\Models;

use App\Enums\RepaymentStatusEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property mixed $paid_amount
 * @property string $status
 */
class Repayment extends Model
{
    use HasFactory;

    public $guarded = [];

    protected $casts = [
        'due_amount' => 'decimal:2',
        'paid_amount' => 'decimal:2',
    ];

    /**
     * @return BelongsTo
     */
    public function user():BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function loan(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param mixed $amount
     * @param Loan $loan
     * @return void
     */
    public function pay(mixed $amount, Loan $loan): void
    {
        $this->paid_amount = $amount;
        $this->status = RepaymentStatusEnum::PAID->value;
        $this->save();

        $loan->updateOnRepayment($amount);
    }

}
