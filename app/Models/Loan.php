<?php

namespace App\Models;

use App\Enums\LoanStatusEnum;
use App\Enums\RepaymentStatusEnum;
use DateTime;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Loan extends Model
{
    use HasFactory;

    public $guarded = [];

    protected $casts = [
        'amount' => 'decimal:2',
        'paid_amount' => 'decimal: 2',
    ];

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function repayments(): HasMany
    {
        return $this->hasMany(Repayment::class);
    }


    /**
     * Generate repayments for a loan
     * @return void
     */
    public function generateRepayments(): void
    {
        $avg = round($this->amount / $this->term, 2);
        $repeat = $this->term - 1;
        $collection = collect([]);
        if($repeat > 0) {
            $collection = collect(array_fill(0, $repeat, $avg));
        }
        $rePaymentAmounts = $collection->push($this->amount - $collection->sum());
        $date = $this->created_at;
        foreach ($rePaymentAmounts as $rePaymentAmount) {
            Repayment::create([
                'loan_id' => $this->id,
                'user_id' => $this->user_id,
                'due_amount' => $rePaymentAmount,
                'due_date'=> $date->addDays(7),
                'paid_amount' => 0,
                'status' => RepaymentStatusEnum::PENDING->value,
            ]);
        }
    }

    /**
     * @param mixed $amount
     * @return void
     */
    public function updateOnRepayment(mixed $amount): void
    {
        $this->paid_amount += $amount;
        $this->save();

        if($this->amount == $this->paid_amount){
            \DB::table('repayments')->where('loan_id', $this->id)
                ->where('status', RepaymentStatusEnum::PENDING->value)
                ->update(['status' => RepaymentStatusEnum::PAID->value]);

            $this->status = LoanStatusEnum::PAID->value;
            $this->save();
        }
    }
}
