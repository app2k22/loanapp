<?php
namespace App\Enums;

enum LoanStatusEnum: string
{
    case APPROVED = 'approved';
    case PENDING = 'pending';
    case PAID = 'paid';
}
