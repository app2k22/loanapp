<?php
namespace App\Enums;

enum RepaymentStatusEnum: string
{
    case PAID = 'paid';
    case PENDING = 'pending';
}
