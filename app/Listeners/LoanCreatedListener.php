<?php

namespace App\Listeners;

use App\Events\LoanCreatedEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class LoanCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param LoanCreatedEvent $event
     * @return void
     */
    public function handle(LoanCreatedEvent $event)
    {
        $event->loan->generateRepayments();
    }
}
