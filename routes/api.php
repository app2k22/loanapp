<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoanController;
use App\Models\Loan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('auth')->group(function(){
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::post('login', [AuthController::class, 'login'])->name('login');
});
Route::middleware('auth:sanctum')->group(function(){
    Route::prefix('loans')->group(function(){
        Route::post('/', [LoanController::class, 'store'])->name('loans.store');
        Route::post('/{loan}/status', [LoanController::class, 'changeStatus'])->middleware('admin')->name('loans.status.post');
        Route::get('/{loan}', [LoanController::class, 'show'])->middleware('admin')->name('loans.show');

        Route::prefix('{loan}/repayments')->group(function(){
            Route::post('/', [\App\Http\Controllers\RepaymentController::class, 'store'])->name('repayments.store');
        });
    });
});
