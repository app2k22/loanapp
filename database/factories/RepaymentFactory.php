<?php

namespace Database\Factories;

use App\Models\Loan;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Repayment>
 */
class RepaymentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'loan_id' => Loan::factory(),
            'due_amount' => function (array $attributes) {
                $loan = Loan::find($attributes['loan_id']);
                return $loan->amount / $loan->term;
            },
            'due_date' => function (array $attributes) {
                $loan = Loan::find($attributes['loan_id']);
                return $loan->created_at->addDays(7);
            },
            'paid_amount' => 0.00,
            'status' => 'pending'
        ];
    }
}
